/**
 * 系统设置
 */

import request from '@/utils/request'
import { api } from '~/ui-domain'

/**
 * 查询sku详情
 */
export function getsupplierSkuGoodsDetail(id) {
  return request({
    url: `${api.supplier}/admin/supplier/sku/${id}`,
    method: 'get',
    loading: false
  })
}

/**
 * 查询所有变更类型
 */
export function skuChangeRecordChangeTypeList(params) {
  return request({
    url: `${api.supplier}/admin/supplier/sku-change-record/change-type-list`,
    method: 'get',
    loading: false,
    params
  })
}

/**
 * 供应商sku变更记录
 */
export function supplierSkuChangeRecord(params) {
  return request({
    url: `${api.supplier}/admin/supplier/sku-change-record`,
    method: 'get',
    loading: false,
    params
  })
}

/**
 * 将供应商sku导入效验
 */
export function supplierGoodsBatchImportCheck(params) {
  return request({
    url: `${api.supplier}/admin/supplier/sku/batch-import-check`,
    method: 'get',
    loading: false,
    params
  })
}

/**
 * 将供应商sku导入到平台商品列表
 */
export function supplierGoodsBatchImport(params) {
  return request({
    url: `${api.supplier}/admin/supplier/sku/batch-import`,
    method: 'get',
    loading: false,
    params
  })
}

/**
 * 分页查询
 */
export function getSupplierGoodsList(data) {
  return request({
    url: `${api.supplier}/admin/supplier/sku`,
    method: 'get',
    loading: false,
    params: data
  })
}

/**
 * 更新运费模板
 */
export function wxTemplate(data) {
  return request({
    url: `${api.admin}/admin/goods/wx-template`,
    method: 'get',
    loading: false,
    params: data
  })
}
/**
 * 全量同步商品
 */
export function supplierFirstInit(type) {
  return request({
    url: `${api.supplier}/admin/supplier/sku/${type}/first-init`,
    method: 'get',
    loading: false
  })
}

/**
 * 修改供应商信息
 */
export function editSupplierList(type, data) {
  return request({
    url: `${api.supplier}/admin/supplier/${type}`,
    method: 'put',
    loading: false,
    headers: { 'Content-Type': 'application/json' },
    data
  })
}

/**
 * 查询所有供应商
 */
export function getSupplierList() {
  return request({
    url: `${api.supplier}/admin/supplier/list-all`,
    method: 'get',
    loading: false
  })
}

/**
 * 修改供应商设置
 * @param params
 */
export function editSupplierSetting(params) {
  return request({
    url: `${api.supplier}/admin/supplier/settings`,
    method: 'POST',
    data: params
  })
}

/**
 * 供应商设置查询
 */
export function getSupplierSetting() {
  return request({
    url: `${api.supplier}/admin/supplier/settings`,
    method: 'get',
    loading: false
  })
}

/**
 * 应用禁用
 */
export function disSystemsApp(id, params) {
  return request({
    url: `admin/systems/app/${id}/disable`,
    method: 'post',
    loading: false,
    params
  })
}

/**
 * 删除应用
 */
export function delSystemsApp(id) {
  return request({
    url: `admin/systems/app/${id}`,
    method: 'delete',
    loading: false
  })
}

/**
 * 添加应用
 */
export function addSystemsApp(params) {
  return request({
    url: 'admin/systems/app',
    method: 'post',
    loading: false,
    params
  })
}

/**
 * 查询应用分页列表数据
 */
export function getSystemsApp(params) {
  return request({
    url: 'admin/systems/app',
    method: 'get',
    loading: false,
    params
  })
}

/**
 * 获取站点设置
 */
export function getSiteSetting() {
  return request({
    url: 'admin/settings/site',
    method: 'get',
    loading: false
  })
}

/**
 * 修改站点设置
 * @param params
 */
export function editSiteSetting(params) {
  return request({
    url: 'admin/settings/site',
    method: 'put',
    data: params
  })
}

/**
 * 获取积分设置
 */
export function getPointSetting() {
  return request({
    url: 'admin/settings/point',
    method: 'get'
  })
}

/**
 * 修改积分设置
 * @param params
 */
export function editPointSetting(params) {
  return request({
    url: 'admin/settings/point',
    method: 'put',
    data: params
  })
}

/**
 * 获取企业微信设置
 */
export function getQiyewxSetting() {
  return request({
    url: `${api.admin}/admin/settings/qiye-weixin`,
    method: 'get'
  })
}

/**
 * 修改企业微信设置
 * @param params
 */
export function editQiyewxSetting(params) {
  return request({
    url: `${api.admin}/admin/settings/qiye-weixin`,
    method: 'put',
    data: params
  })
}

/**
 * 获取供应商同步记录
 */
export function getSupplierMessageList(params) {
  return request({
    url: `${api.supplier}/admin/supplier/message`,
    method: 'get',
    params
  })
}

/**
 * 获取供应商消息类型
 */
export function getSupplierMessageTypeList() {
  return request({
    url: `${api.supplier}/admin/supplier/message/type-list`,
    method: 'get'
  })
}
