/**
 * 商品调拨相关API
 */

import request from '@/utils/request'

/**
 * 批量退回商品调拨
 * @param params
 */
export function rejectGoodsTransfer(ids, parmas) {
  return request({
    url: `admin/erp/stockTransfer/${ids}/reject?handle_by_id=${parmas.in_handled_by_id}&handle_by_id_name=${parmas.in_handled_by_name}`,
    method: 'post'
  })
}
/**
 * 批量确认商品调拨
 * @param params
 */
export function successGoodsTransfer(ids, parmas) {
  return request({
    url: `admin/erp/stockTransfer/${ids}/confirm?handle_by_id=${parmas.in_handled_by_id}&handle_by_id_name=${parmas.in_handled_by_name}`,
    method: 'post'
  })
}
/**
 * 提交商品调拨
 * @param params
 */
export function submitGoodsTransfer(id) {
  return request({
    url: `admin/erp/stockTransfer/${id}/submit`,
    method: 'post'
  })
}
/**
 * 撤回商品调拨
 * @param params
 */
export function withdrawGoodsTransfer(id) {
  return request({
    url: `admin/erp/stockTransfer/${id}/withdraw`,
    method: 'post'
  })
}
/**
 * 审核商品调拨
 * @param params
 */
export function auditGoodsTransfer(id, parmas) {
  return request({
    url: `admin/erp/stockTransfer/${id}/audit`,
    method: 'post',
    data: parmas
  })
}

/**
 * 获取商品调拨详情
 * @param params
 */
export function getGoodsTransferInfo(id) {
  return request({
    url: `admin/erp/stockTransfer/${id}`,
    method: 'get',
    loaidng: false
  })
}

/**
 * 获取商品调拨提交列表
 * @param params
 */
export function getGoodsTransferApplyList(params) {
  return request({
    url: 'admin/erp/stockTransfer',
    method: 'get',
    loaidng: false,
    params
  })
}

/**
 * 获取商品调拨确认列表
 * @param params
 */
export function getGoodsTransferConfirmList(params) {
  return request({
    url: 'admin/erp/stockTransfer/confirm-list',
    method: 'get',
    loaidng: false,
    params
  })
}

/**
 * 添加商品调拨
 * @param params
 */
export function addGoodsTransfer(parmas) {
  return request({
    url: `admin/erp/stockTransfer`,
    method: 'post',
    headers: { 'Content-Type': 'application/json' },
    data: parmas
  })
}

/**
 * 更新商品调拨
 * @param id
 * @param parmas
 */
export function editGoodsTransfer(id, parmas) {
  return request({
    url: `admin/erp/stockTransfer/${id}`,
    method: 'put',
    headers: { 'Content-Type': 'application/json' },
    data: parmas
  })
}

/**
 * 删除商品调拨
 * @param id
 */
export function deleteGoodsTransfer(ids) {
  return request({
    url: `admin/erp/stockTransfer/${ids}`,
    method: 'delete'
  })
}
