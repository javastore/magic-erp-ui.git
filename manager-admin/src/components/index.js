import UploadSortable from './UploadSortable'
import UE from './UE'
const components = {
  UploadSortable,
  UE
}

components.install = function(Vue) {
  Object.keys(components).forEach(function(key) {
    key !== 'install' && Vue.component(components[key].name, components[key])
  })
}

export default components

export {
  UploadSortable,
  UE
}
